import sqlite3
from sqlite3 import Error

class SqliteConnection(object):

    """Docstring for SqliteConnection. """

    def __init__(self, db_file):
        """ create a database connection to a database that resides
            in the memory
        """
        try:
            self.conn = sqlite3.connect(db_file)
        except Error as exc:
            print(exc)
    
    def write_name(self, name, count_givenname, count_surname):
        """
        Insert A name with counts into the database
        """
        sql = ''' INSERT INTO projects(name,count_givenname,count_surname) VALUES(?,?,?)  '''
        cur = self.conn.cursor()
        cur.execute(sql, name, count_givenname, count_surname)
        return cur.lastrowid

    def __del__(self):
        self.conn.close()

import requests
import csv
import sqlite3
import database_connection
from bs4 import BeautifulSoup

def main():
    """
    Main function
    """
    with open('given_and_surnames.csv', 'r') as csvfile:
        conn = database_connection.SqliteConnection
        namereader = csv.DictReader(csvfile, delimiter=',')
        for row in namereader:
            url = row['Link']
            name = row['Name']
            page = requests.get(url)
            soup = BeautifulSoup(page.text)

if __name__ == "__main__":
    main()

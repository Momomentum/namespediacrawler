import requests
import csv
from bs4 import BeautifulSoup

def main():

    """
    The main function
    """

    base_url = 'http://de.namespedia.com'
    upper_alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    lower_alphabet = 'abcdefghijklmnopqrstuvwxyz'
    page_urls = []
    file_handle = csv.writer(open('given_and_surnames.csv', 'w'))
    file_handle.writerow(['Name', 'Link'])
    for upper_letter in upper_alphabet:
        for lower_letter in lower_alphabet:
            url = base_url + '/list/' + upper_letter + lower_letter + '1'
            print(url)
            page = requests.get(url)
            soup = BeautifulSoup(page.text, 'html.parser')
            content = soup.find('div', id='content')
            children = content.findChildren('a')
            counter = 0
            for child in children:
                if child.get('href').startswith('/list'):
                    counter += 1
                else:
                    name_url = base_url + child.get('href')
                    page_urls.append(name_url)
                    file_handle.writerow([child.get('href').split('/')[-1], name_url])

            for i in range(1, counter):
                url = base_url + '/list/' + upper_letter + lower_letter + str(i)
                print(url)
                page = requests.get(url)
                soup = BeautifulSoup(page.text, 'html.parser')
                content = soup.find('div', id='content')
                children = content.findChildren('a')
                for child in children:
                    if child.get('href').startswith('/details'):
                        name_url = base_url + child.get('href')
                        page_urls.append(name_url)
                        file_handle.writerow([child.get('href').split('/')[-1], name_url])


if __name__ == "__main__":
    main()

class Name(object):

    """Docstring for Name. """

    def __init__(self):
        """TODO: to be defined1. """


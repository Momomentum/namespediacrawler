import requests
from bs4 import BeautifulSoup


def get_name_infos(namespedia_page):
    """TODO: Docstring for get_name_infos.

    :namespedia_page: TODO
    :returns: TODO

    """
    soup = BeautifulSoup(namespedia_page.text, 'html.parser')
    content = soup.find('div', id='content')
    bold = content.findChildren('b')
    name = bold[0].getText()
    count_givenname = int(bold[0].next_sibling.split()[4])
    count_surname = int(bold[1].next_sibling.split()[4])
    return name, count_givenname, count_surname


if __name__ == "__main__":
    page = requests.get("http://namespedia.com/details/Agatta")
    name, giv, sur = get_name_infos(page)
    print("Name: %s, GivenCount: %d, SurCount: %d" % (name, giv, sur))
